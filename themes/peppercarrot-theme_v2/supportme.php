<?php if(!defined('PLX_ROOT')) exit;?>

    <div style="max-width: 640px; margin: 0 auto;"><?php $plxShow->lang('PHILOSOPHY_DONATION') ?></div>
    <?php $plxShow->lang('HOMEPAGE_ALTERNATIVES') ?>
    <a href="https://paypal.me/davidrevoy" title="Send money via Paypal">Paypal</a>, 
    <a href="https://www.tipeee.com/pepper-carrot" title="<?php $plxShow->lang('HOMEPAGE_PATREON_BOX') ?> Tipeee">Tipeee</a>,
    <a href="https://liberapay.com/davidrevoy/" title="<?php $plxShow->lang('HOMEPAGE_PATREON_BOX') ?> Liberapay">Liberapay</a>,
    <a href="https://g1.duniter.fr/#/app/wot/4nosBEwT8xQfMY11sq32AnZF1XcoqzG9tArXJq9mu8Wc/DavidRevoy" title="Send G1 to David Revoy">G1</a>,
    <a href="<?php $plxShow->urlRewrite('?static12/iban-and-mail-adress') ?>" title="Send money via IBAN or Send goods via mail">Bank/Mail</a>.
